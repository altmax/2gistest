# Test API

server run on http://185.111.219.235:3000

# API Description

https://app.swaggerhub.com/apis/cialldoin/2gistest/1.0.0

# Build

```
go get https://bitbucket.org/altmax/2gistest.git
go build
```

# Run

```
./2gistest --config=path/to/config/toml
```

Configs template - config.template.toml

# Migrations

- migration/0001_init.up.sql - use for cretion database structurs(tables)
- migration/testdata.sql - generated tests data
- migration/0001_init.down.sql - use for drop tables

# Generating test data

```
go run migration/testgen.go - save generated data to migration/testdata.sql
```

## TestGen vars

- maxDeepLevel - maximum amount of level descendant
- maxChildCount - maximum amount of ancestors child
- maxFirstHeadingLevelChildCount -  maximum amount of ancestors child on first level
- maxStreetCount - maximum amount of streets
- maxStreetBuildingCount - maximum amount of buildings on each street
- maxFirmCount - maximum amount of firms
- maxFirmHeading - maximum amount of firms heading
- maxFirmPhone - maximum amount of phones for each firm


# TODO

- Tests
- More comments
- Optimization queryes to DB
- Dockerfile
- Log to file
- More detailed description API via swagger
- Vendor(via govendor or glide)
