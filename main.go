package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/altmax/2gistest/server"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	configPath = flag.String("config", "", "Configuration file path")

	mysqlMaster = flag.String(
		"mysql-master",
		"",
		"Address of master instance of MySQL",
	)
	mysqlSlave = flag.String(
		"mysql-slave",
		"",
		"Address of slave instance of MySQL",
	)

	port     = flag.String("port", "3000", "Port to bind to")
	hostname = flag.String("hostname", "localhost", "Hostname to bind to")

	profile = flag.String(
		"profile", "",
		"Enable CPU profiling for every request or via net/http/pprof",
	)
)

func main() {
	flag.Parse()

	config := viper.New()
	config.SetConfigType("toml")

	if *configPath != "" {
		configFile, err := os.Open(*configPath)
		if err != nil {
			log.Fatalf("opening config failed: %s", err)
		}

		err = config.ReadConfig(configFile)
		if err != nil {
			log.Fatalf("reading config failed: %s", err)
		}
	}

	config.BindPFlag("port", flag.Lookup("port"))
	config.BindPFlag("hostname", flag.Lookup("hostname"))
	config.BindPFlag("mysql.master", flag.Lookup("mysql-master"))
	config.BindPFlag("mysql.slave", flag.Lookup("mysql-slave"))
	config.BindPFlag("profile", flag.Lookup("profile"))

	s := server.New(config)
	data, err := json.MarshalIndent(s.Routes(), "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	ioutil.WriteFile("routes.json", data, 0644)

	listenOn := config.GetString("hostname") + ":" + config.GetString("port")
	s.Logger.Fatal(s.Start(listenOn))
}
