-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'building'
-- 
-- ---

-- DROP TABLE IF EXISTS building;
		
CREATE TABLE IF NOT EXISTS building (
  id BIGINT NOT NULL AUTO_INCREMENT,
  street VARCHAR(255) NOT NULL,
  number INT NOT NULL,
  longitude FLOAT(11, 8) NOT NULL,
  latitude FLOAT(11, 8) NOT NULL,
  PRIMARY KEY (id)
);

-- ---
-- Table 'firm'
-- 
-- ---

-- DROP TABLE IF EXISTS firm;
		
CREATE TABLE IF NOT EXISTS firm (
  id BIGINT NOT NULL AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  building_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY(building_id) REFERENCES building(id)
);

-- ---
-- Table 'heading'
-- 
-- ---

-- DROP TABLE IF EXISTS heading;
		
CREATE TABLE IF NOT EXISTS heading (
  id BIGINT NOT NULL AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  parent_id BIGINT NULL DEFAULT NULL,
  PRIMARY KEY (id)
);


-- ---
-- Table 'heading_tree'
-- 
-- ---

-- DROP TABLE IF EXISTS heading;
		
CREATE TABLE IF NOT EXISTS heading_tree (
  id BIGINT NOT NULL AUTO_INCREMENT,
  id_anc BIGINT NOT NULL, 
  id_desc BIGINT NOT NULL, 
  id_na BIGINT NOT NULL, 
  level INT NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);

-- ---
-- Table 'firm_headings'
-- 
-- ---

-- DROP TABLE IF EXISTS firm_headings;
		
CREATE TABLE IF NOT EXISTS firm_headings (
  id BIGINT NOT NULL AUTO_INCREMENT,
  firm_id BIGINT NOT NULL,
  heading_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY(firm_id) REFERENCES firm(id),
  CONSTRAINT FOREIGN KEY(heading_id) REFERENCES heading(id)
);

-- ---
-- Table 'phones'
-- 
-- ---

-- DROP TABLE IF EXISTS phones;
		
CREATE TABLE IF NOT EXISTS phone (
  id BIGINT NOT NULL AUTO_INCREMENT,
  phone VARCHAR(31) NOT NULL,
  firm_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY(firm_id) REFERENCES firm(id)
);

-- ---
-- Foreign Keys 
-- ---

-- ALTER TABLE firm ADD FOREIGN KEY (building_id) REFERENCES building (id);
-- ALTER TABLE heading ADD FOREIGN KEY (parent) REFERENCES heading (id);
-- ALTER TABLE firm_headings ADD FOREIGN KEY (firm_id) REFERENCES firm (id);
-- ALTER TABLE firm_headings ADD FOREIGN KEY (heading_id) REFERENCES heading (id);
-- ALTER TABLE phones ADD FOREIGN KEY (id) REFERENCES firm (id);

-- ---
-- Table Properties
-- ---

ALTER TABLE firm ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE building ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE heading ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE heading_tree ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE firm_headings ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE phone ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO firm (id,title,building_id) VALUES
-- ('','','');
-- INSERT INTO building (id,street,number,longitude,latitude) VALUES
-- ('','','','','');
-- INSERT INTO heading (id,title,parent) VALUES
-- ('','','');
-- INSERT INTO firm_headings (id,firm_id,heading_id) VALUES
-- ('','','');
-- INSERT INTO phones (id,phone,firm_id) VALUES
-- ('','','');