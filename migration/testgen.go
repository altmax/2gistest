package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"sync"

	"bitbucket.org/altmax/2gistest/models"
)

var (
	curID                          = &Count{}
	maxDeepLevel                   = 4
	maxChildCount                  = 3
	maxFirstHeadingLevelChildCount = 3
	maxStreetCount                 = 100
	maxStreetBuildingCount         = 100
	maxFirmCount                   = 10000
	maxFirmHeading                 = 3
	maxFirmPhone                   = 2
)

type Count struct {
	cur int64
	rwm sync.RWMutex
}

func (c *Count) Get() int64 {
	// c.rwm.Lock()
	// defer c.rwm.Unlock()
	// defer c.inc()
	return c.inc()
}

func (c *Count) inc() int64 {
	c.rwm.Lock()
	defer c.rwm.Unlock()
	old := c.cur
	c.cur++
	return old
}

func main() {
	_ = runtime.GOMAXPROCS(256)

	if _, err := os.Stat("testdata.sql"); !os.IsNotExist(err) {
		err := os.Remove("testdata.sql")
		if err != nil {
			log.Fatal(err)
		}
	}

	file, err := os.OpenFile("testdata.sql", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	l := log.New(os.Stderr, "", 0)
	l.SetOutput(file)
	defer file.Close()

	// root := models.HeadingAsDLList{ID: curID.Get(), ParentID: -1, Title: "catalog"}
	root := models.HeadingAsDLList{ID: curID.Get(), Parent: nil, Title: "catalog"}
	var wg sync.WaitGroup
	wg.Add(1)
	FillHeadingChilds(&root, 0, &wg)
	wg.Wait()
	// for _, v := range root.Childs {
	// 	fmt.Println(v.Parent.ID)
	// }
	// fmt.Printf("%#v", root)
	data, err := json.MarshalIndent(&root, "", " ")
	if err != nil {
		log.Fatal(err)
	}
	ioutil.WriteFile("headings.json", data, 0666)

	fmt.Println(curID.cur)

	wg = sync.WaitGroup{}
	wg.Add(1)
	SaveHeadingAsSQL(&root, l, &wg)
	wg.Wait()

	hdgs := root.ToSimpleHeading()
	saveHeadingTree(l, &root)
	blgs := createAndSaveTestBuildings(l)

	firms := createTestFirms(blgs, hdgs)

	saveFirmsAsSQL(firms, l)

}

func saveHeadingTree(l *log.Logger, root *models.HeadingAsDLList, parents ...int64) {
	// fmt.Println(parents)

	if len(parents) > 0 {
		for _, v := range parents {
			l.Printf(`%s`, insertToHdgTree(v, root.ID, root.Parent.ID, root.Level))
		}
	}
	if root.Parent != nil {
		l.Printf(`%s`, insertToHdgTree(root.ID, root.ID, root.Parent.ID, root.Level))
	}
	wg := sync.WaitGroup{}
	for _, v := range root.Childs {
		wg.Add(1)
		go func(wg *sync.WaitGroup, hdg *models.HeadingAsDLList) {
			defer wg.Done()
			newparents := parents
			if hdg.Parent.Parent != nil {
				newparents = append(newparents, root.ID)
			}
			saveHeadingTree(l, hdg, newparents...)
		}(&wg, v)
	}
	wg.Wait()
}

func SaveHeadingAsSQL(root *models.HeadingAsDLList, l *log.Logger, wg *sync.WaitGroup) {
	if root.Parent != nil {
		l.Printf(`%s`, insertToHdg(root.Title, root.ID, root.Parent.ID))
	}
	if len(root.Childs) > 0 {
		for _, v := range root.Childs {
			wg.Add(1)
			go SaveHeadingAsSQL(v, l, wg)
		}
	}
	wg.Done()
}

func FillHeadingChilds(root *models.HeadingAsDLList, curLevel int, wg *sync.WaitGroup) {
	if curLevel == maxDeepLevel {
		root.Childs = nil
		wg.Done()
		return
	}
	maxCC := rand.Intn(maxChildCount)
	if curLevel == 0 {
		maxCC = maxFirstHeadingLevelChildCount
	}
	for i := 0; i < maxCC; i++ {
		hid := curID.Get()
		// h := &models.HeadingAsDLList{ID: hid, ParentID: root.ID, Title: "level" + strconv.Itoa(curLevel) + "heading" + strconv.FormatInt(hid, 10)}
		h := &models.HeadingAsDLList{ID: hid, Parent: root, Title: "heading" + strconv.FormatInt(hid, 10), Level: curLevel}
		root.Childs = append(root.Childs, h)
		wg.Add(1)
		go FillHeadingChilds(h, curLevel+1, wg)
	}
	wg.Done()
}

func createAndSaveTestBuildings(l *log.Logger) []*models.Building {
	bldgs := make([]*models.Building, maxStreetBuildingCount*maxStreetCount)
	k := 0
	for i := 0; i < maxStreetCount; i++ {
		for j := 0; j < maxStreetBuildingCount; j++ {
			offset := rand.Float64()
			blg := models.NewBuilding(
				int64(k+1),
				"street"+strconv.Itoa(i+1),
				j+1,
				82.91667+ternary(i%2 == 0, offset, -offset).(float64),
				55.01667+ternary(j%2 == 0, offset, -offset).(float64),
			)
			bldgs[k] = blg
			k++
			l.Printf(`%s`, insertToBlg(blg.ID, blg.Address.Street, blg.Address.Number, blg.Coordinates.Longitude, blg.Coordinates.Latitude))
		}
	}
	return bldgs
}

func createTestFirms(blgs []*models.Building, hdgs []*models.Heading) []*models.Firm {
	firms := make([]*models.Firm, maxFirmCount)

	for i := 0; i < maxFirmCount; i++ {
		randBlg := blgs[rand.Intn(len(blgs)-1)+1]

		hnums := make([]int, 0)
		randHdgs := make([]*models.Heading, 0)
		fhcount := rand.Intn(maxFirmHeading-1) + 1
		for len(hnums) != fhcount {
			rndNum := rand.Intn(len(hdgs))

			ex := false
			for _, v := range hnums {
				if v == rndNum {
					ex = true
					break
				}
			}
			if !ex {
				hnums = append(hnums, rndNum)
				randHdgs = append(randHdgs, hdgs[rndNum])
			}
		}

		//Phone format - xxx-xxx-xxx
		rndPhones := make([]*models.Phone, maxFirmPhone)
		for j := 0; j < maxFirmPhone; j++ {
			phone := ""
			for k := 0; k < 3; k++ {
				ph := rand.Intn(1000)

				strPh := strconv.Itoa(ph)
				for len(strPh) < 3 {
					strPh += "0"
				}
				phone += strPh

				phone += ternary(k != 2, "-", "").(string)
			}
			rndPhones[j] = &models.Phone{Phone: phone}
		}

		firms[i] = &models.Firm{
			ID:       int64(i + 1),
			Title:    "firm" + strconv.Itoa(i+1),
			Building: randBlg,
			Phones:   rndPhones,
			Headings: randHdgs,
		}

	}
	return firms
}

func saveFirmsAsSQL(firms []*models.Firm, l *log.Logger) {
	for _, v := range firms {
		l.Printf(`%s`, insertToFirm(v.Title, v.Building.ID, v.ID))
		for _, p := range v.Phones {
			l.Printf(`%s`, insertToPhone(p.Phone, v.ID))
		}
		for _, h := range v.Headings {
			l.Printf(`%s`, insertToFirmHdg(v.ID, h.ID))
		}
	}
}

func ternary(condition bool, trueRet, flaseRet interface{}) interface{} {
	if condition {
		return trueRet
	}
	return flaseRet
}

func insertToFirm(title string, blgID, id int64) string {
	return fmt.Sprintf(`insert into firm (id, title, building_id) values (%d, '%s', %d);`, id, title, blgID)
}

func insertToBlg(id int64, street string, number int, lgt, ltt float64) string {
	return fmt.Sprintf(`insert into building (id, street, number, longitude, latitude) values (%d, '%s', %d, %f, %f);`, id, street, number, lgt, ltt)
}

func insertToHdg(title string, id, parent int64) string {
	return fmt.Sprintf(`insert into heading (id, title, parent_id) values(%d, '%s', %d);`, id, title, parent)
}

func insertToHdgTree(idAnc, idDesc, idNA int64, level int) string {
	return fmt.Sprintf(`insert into heading_tree (id_anc, id_desc, id_na, level) values(%d, %d, %d, %d);`, idAnc, idDesc, idNA, level)
}

func insertToFirmHdg(firmID, hdgID int64) string {
	return fmt.Sprintf(`insert into firm_headings (firm_id, heading_id) values (%d, %d);`, firmID, hdgID)
}

func insertToPhone(phone string, firmID int64) string {
	return fmt.Sprintf(`insert into phone (phone, firm_id) values ('%s', %d);`, phone, firmID)
}
