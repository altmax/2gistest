package models

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type Building struct {
	ID          int64 `xml:"id" json:"id,omitempty" db:"id"`
	Address     *Address
	Coordinates *Coordinates
}
type modelBuilding struct {
	ID        int64   `xml:"id" json:"id,omitempty" db:"id"`
	Street    string  `xml:"street" json:"street,omitempty" db:"street"`
	Number    int     `xml:"number" json:"number,omitempty" db:"number"`
	Longitude float64 `xml:"longitude" json:"longitude,omitempty" db:"longitude"`
	Latitude  float64 `xml:"latitude" json:"latitude,omitempty" db:"latitude"`
}

type Address struct {
	Street string `xml:"street" json:"street,omitempty" db:"street"`
	Number int    `xml:"number" json:"number,omitempty" db:"number"`
}

type Coordinates struct {
	Longitude float64 `xml:"longitude" json:"longitude,omitempty" db:"longitude"`
	Latitude  float64 `xml:"latitude" json:"latitude,omitempty" db:"latitude"`
}

func NewBuilding(id int64, street string, num int, lgt, ltt float64) *Building {
	return &Building{
		ID: id,
		Address: &Address{
			Street: street,
			Number: num,
		},
		Coordinates: &Coordinates{
			Longitude: lgt,
			Latitude:  ltt,
		},
	}
}

type BuildingTable struct {
	DB interface {
		sqlx.Execer
		Get(interface{}, string, ...interface{}) error
		Select(interface{}, string, ...interface{}) error
	}
}

func (table *BuildingTable) FindAll() ([]Building, error) {
	query := `select id, street, number, longitude, latitude 
				from building;`

	var mbldgs []modelBuilding

	err := table.DB.Select(&mbldgs, query)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}
	bldgs := make([]Building, len(mbldgs))
	for i, v := range mbldgs {
		b := NewBuilding(v.ID, v.Street, v.Number, v.Longitude, v.Latitude)
		bldgs[i] = *b
	}

	return bldgs, nil
}

func (table *BuildingTable) FindByID(id int64) (*Building, error) {
	query := `select id, street, number, longitude, latitude 
				from building
				where id = ?;`

	var mbldg modelBuilding

	err := table.DB.Get(&mbldg, query, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}
	b := NewBuilding(mbldg.ID, mbldg.Street, mbldg.Number, mbldg.Longitude, mbldg.Latitude)

	return b, nil
}

func (table *BuildingTable) Exist(id int64) (bool, error) {
	query := `select id 
				from building 
				where id = ?;`

	var i int64

	err := table.DB.Get(&i, query, id)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, errors.Wrap(err, "get from db failed")
	}

	return true, nil
}

func (table *BuildingTable) FindByAddress(street string, num int) (*Building, error) {
	query := `select id, street, number, longitude, latitude 
				from building `
	where := ""
	args := make([]interface{}, 0)
	if street != "" {
		where = "street = ?"
		args = append(args, street)
	}
	if num > 0 {
		if where != "" {
			where += " and number = ?"
		} else {
			where = "number = ?"
		}
		args = append(args, num)
	}
	if where != "" {
		query += where
	}
	query += ";"
	var mbldg modelBuilding

	err := table.DB.Get(&mbldg, query, args...)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}
	b := NewBuilding(mbldg.ID, mbldg.Street, mbldg.Number, mbldg.Longitude, mbldg.Latitude)
	return b, nil
}

func (table *BuildingTable) FindByRect(x1, x2, y1, y2 float64) ([]Building, error) {
	query := `select b.id, b.street, b.number, b.longitude, b.latitude 
				from building as b
				where b.latitude > ? and b.latitude < ? and b.longitude > ? and b.longitude < ?;`

	var mbldgs []modelBuilding

	err := table.DB.Select(&mbldgs, query, x1, x2, y1, y2)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}
	bldgs := make([]Building, len(mbldgs))
	for i, v := range mbldgs {
		b := NewBuilding(v.ID, v.Street, v.Number, v.Longitude, v.Latitude)
		bldgs[i] = *b
	}

	return bldgs, nil
}
