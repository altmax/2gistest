package models

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"
)

//Firm is full struct of firm model
type Firm struct {
	ID       int64  `xml:"id" json:"id,omitempty" db:"id"`
	Title    string `xml:"title" json:"title,omitempty" db:"title"`
	Phones   []*Phone
	Building *Building
	Headings []*Heading
}

//firmModel is model as db table
type firmModel struct {
	ID         int64  `xml:"id" json:"id,omitempty" db:"id"`
	Title      string `xml:"title" json:"title,omitempty" db:"title"`
	Phones     string `xml:"phones" json:"phones,omitempty" db:"phones"`
	BuildingID int64  `xml:"building_id" json:"building_id,omitempty" db:"building_id"`
	Headings   string `xml:"headings" json:"headings,omitempty" db:"headings"`
	Street     string `xml:"street" json:"street,omitempty" db:"street"`
	Number     int    `xml:"number" json:"number,omitempty" db:"number"`
}

//ToFirm convert table_model to full_model
func (firm firmModel) ToFirm() Firm {
	var phones []*Phone
	if firm.Phones != "" {
		phs := strings.Split(firm.Phones, ",")
		phones = make([]*Phone, len(phs))
		for i, v := range phs {
			phones[i] = &Phone{Phone: v}
		}
	}

	var headings []*Heading
	if firm.Headings != "" {
		hdgs := strings.Split(firm.Headings, ",")
		headings = make([]*Heading, len(hdgs))
		for i, v := range hdgs {
			hdg := strings.Split(v, ":")
			hdgID, _ := strconv.ParseInt(hdg[0], 10, 64)
			headings[i] = &Heading{ID: hdgID, Title: hdg[1]}
		}
	}

	f := Firm{
		ID:       firm.ID,
		Title:    firm.Title,
		Building: NewBuilding(firm.BuildingID, firm.Street, firm.Number, 0, 0),
		Phones:   phones,
		Headings: headings,
	}
	return f
}

//Phone is
type Phone struct {
	ID    int64  `xml:"id" json:"id,omitempty" db:"id"`
	Phone string `xml:"phone" json:"phone,omitempty" db:"phone"`
}

//FirmTable is struct for using sql.Driver
type FirmTable struct {
	DB interface {
		sqlx.Execer
		Get(interface{}, string, ...interface{}) error
		Select(interface{}, string, ...interface{}) error
	}
}

//FullFindAll returns all firms with full information
func (table *FirmTable) FullFindAll() ([]Firm, error) {
	query := `select sel.id, sel.title, sel.phones, sel.building_id, sel2.headings, sel.street, sel.number
				from 
					(
						select f.id, f.title, group_concat(ph.phone) as phones, f.building_id, b.street, b.number
							from 
								firm as f 
									join phone as ph on ph.firm_id=f.id, 
								building as b where b.id=f.building_id 
							group by f.id
					) as sel,
					(
						select f.id, group_concat(concat(hdg.id, ":", hdg.title)) as headings 
						from firm as f 
							join firm_headings as fh on fh.firm_id=f.id 
								join heading as hdg on hdg.id=fh.heading_id 
						group by f.id
					) as sel2
				where sel.id=sel2.id 
				group by sel.id;`

	var mfirms []firmModel

	err := table.DB.Select(&mfirms, query)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	firms := make([]Firm, len(mfirms))
	for i, firm := range mfirms {

		firms[i] = firm.ToFirm()
	}

	return firms, nil

}

//FindAll returns all firms
func (table *FirmTable) FindAll() ([]Firm, error) {
	query := `select id, title, building_id
				from firm;`

	var mfirms []firmModel

	err := table.DB.Select(&mfirms, query)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	firms := make([]Firm, len(mfirms))
	for i, firm := range mfirms {

		firms[i] = firm.ToFirm()
	}

	return firms, nil

}

//FindByID returns firm with this ID
func (table *FirmTable) FindByID(id int64) (*Firm, error) {
	query := `select sel.id, sel.title, sel.phones, sel.building_id, sel2.headings, sel.street, sel.number
				from 
					(
						select f.id, f.title, group_concat(ph.phone) as phones, f.building_id, b.street, b.number
							from 
								firm as f 
									join phone as ph on ph.firm_id=f.id, 
								building as b where b.id=f.building_id  and f.id = ?
							group by f.id
					) as sel,
					(
						select f.id, group_concat(concat(hdg.id, ":", hdg.title)) as headings 
						from firm as f 
							join firm_headings as fh on fh.firm_id=f.id 
								join heading as hdg on hdg.id=fh.heading_id and f.id = ?
						group by f.id
					) as sel2
				where sel.id=sel2.id
				group by sel.id;`

	var firm firmModel

	err := table.DB.Get(&firm, query, id, id)
	if err == sql.ErrNoRows {
		fmt.Println("no rows")
		return nil, nil
	}
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	f := firm.ToFirm()
	fmt.Println(f)

	return &f, nil
}

//FindByTitle returns firm with this name
func (table *FirmTable) FindByTitle(title string) (*Firm, error) {
	query := `select f.id, f.title, f.building_id, b.street, b.number
				from 
					firm as f , 
					building as b 
					where b.id=f.building_id and f.title = ?
				group by f.id;`

	var firm firmModel

	err := table.DB.Get(&firm, query, title, title)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	f := firm.ToFirm()

	return &f, nil
}

//FindByBuildingID returns firms where building_id = this_id
func (table *FirmTable) FindByBuildingID(blgID int64) ([]Firm, error) {
	query := `select f.id, f.title, b.street, b.number
				from 
					firm as f 
					join building as b on b.id=f.building_id
					where f.building_id = ?
				group by f.id;`

	var mfirms []firmModel

	err := table.DB.Select(&mfirms, query, blgID)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	firms := make([]Firm, len(mfirms))
	for i, firm := range mfirms {

		firms[i] = firm.ToFirm()
	}

	return firms, nil
}

func (table *FirmTable) FindByRad(x1, x2, y1, y2, lat, long, dist float64) ([]Firm, error) {
	setVars := `set @dist = ?;`
	_, err := table.DB.Exec(setVars, dist)
	setVars = `set @lat = ?;`
	_, err = table.DB.Exec(setVars, lat)
	setVars = `set @lon = ?;`
	_, err = table.DB.Exec(setVars, long)
	query := `select f.id, f.title
				from 
					firm as f,
					(select b.id, 3956*1609.34*2*asin(
												sqrt(
													power(
														sin((@lat-b.latitude)*pi()/180/2), 2
													) +
													cos(@lat*pi()/180)*
													cos(b.latitude*pi()*180)*
													power(
														sin((@lon-b.longitude)*pi()/180/2), 2
													)
												)
											) as distance
						from building as b
						where 
						b.latitude between ? and ?
						and 
						b.longitude between ? and ?) as sel

					where sel.id = f.id and sel.distance < @dist
				group by f.id;`

	var mfirms []firmModel

	err = table.DB.Select(&mfirms, query, x1, x2, y1, y2)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	firms := make([]Firm, len(mfirms))
	for i, firm := range mfirms {

		firms[i] = firm.ToFirm()
	}

	return firms, nil
}

//FindByRect returns firms with building_id within this rectangle
func (table *FirmTable) FindByRect(x1, x2, y1, y2 float64) ([]Firm, error) {
	query := `select f.id, f.title
				from 
					firm as f
					where f.building_id in(select b.id 
											from building as b
											where 
											b.latitude between ? and ?
											and 
											b.longitude between ? and ?)
				group by f.id;`

	var mfirms []firmModel

	err := table.DB.Select(&mfirms, query, x1, x2, y1, y2)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	firms := make([]Firm, len(mfirms))
	for i, firm := range mfirms {

		firms[i] = firm.ToFirm()
	}

	return firms, nil
}

//FindByHeadingID return all firms with a heading_id = this ID. Supports pagination by limit and offset
func (table *FirmTable) FindByHeadingID(limit, offset int64, hdgID int64) ([]Firm, error) {
	query := `select f.id, f.title, group_concat(concat(hdg.id, ":", hdg.title)) as headings, b.street, b.number 
				from firm as f
					join building as b on b.id=f.building_id
					join firm_headings as fh on fh.firm_id=f.id 
						join heading as hdg on hdg.id=fh.heading_id
									where hdg.id = ?
				group by f.id`

	args := make([]interface{}, 1)
	args[0] = hdgID
	if limit != 0 {
		query += " limit ?"
		args = append(args, limit)
		if offset != 0 {
			query += " offset ?"
			args = append(args, offset)
		}
	}

	query += ";"

	var mfirms []firmModel

	err := table.DB.Select(&mfirms, query, args...)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	firms := make([]Firm, len(mfirms))
	for i, firm := range mfirms {

		firms[i] = firm.ToFirm()
	}

	return firms, nil
}

//DeepFindByHeadingID returns all firms with a heading_id = this ID and his descendants. Supports pagination by limit and offset
func (table *FirmTable) DeepFindByHeadingID(limit, offset int64, hdgID int64) ([]Firm, error) {
	query := `select f.id, f.title, group_concat(concat(hdg.id, ":", hdg.title)) as headings, b.street, b.number 
				from firm as f
					join building as b on b.id=f.building_id
					join firm_headings as fh on fh.firm_id=f.id 
						join heading as hdg on hdg.id=fh.heading_id
							join heading_tree as tree on tree.id_desc=hdg.id 
								where tree.id_anc = ? 
				group by f.id`

	args := make([]interface{}, 1)
	args[0] = hdgID
	if limit != 0 {
		query += " limit ?"
		args = append(args, limit)
		if offset != 0 {
			query += " offset ?"
			args = append(args, offset)
		}
	}

	query += ";"

	var mfirms []firmModel

	err := table.DB.Select(&mfirms, query, args...)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	firms := make([]Firm, len(mfirms))
	for i, firm := range mfirms {

		firms[i] = firm.ToFirm()
	}

	return firms, nil
}
