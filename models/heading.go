package models

import (
	"database/sql"
	"encoding/json"
	"sync"

	"github.com/jmoiron/sqlx"
	// Use MySQL driver
	// _ "github.com/go-sql-driver/mysql"
)

//HeadingAsDLList is doubly linked list struct
type HeadingAsDLList struct {
	ID     int64
	Parent *HeadingAsDLList
	Title  string
	Childs []*HeadingAsDLList
	Level  int
}

func (heading *HeadingAsDLList) MarshalJSON() ([]byte, error) {
	// h := *heading
	type head struct {
		ID       int64
		ParentID int64
		Title    string
		Childs   []*HeadingAsDLList
		Level    int
	}
	h := head{}
	h.ID = heading.ID
	h.ParentID = int64(-1)
	if heading.Parent != nil {
		h.ParentID = heading.Parent.ID
	}
	h.Level = heading.Level
	h.Title = heading.Title
	h.Childs = heading.Childs
	return json.Marshal(h)
}

type headingList struct {
	sync.RWMutex
	slice []*Heading
}

func NewHL() *headingList {
	hl := &headingList{}
	hl.slice = make([]*Heading, 0)
	return hl
}

func (hl *headingList) append(items ...*Heading) {
	hl.Lock()
	defer hl.Unlock()
	hl.slice = append(hl.slice, items...)
}

func (heading *HeadingAsDLList) ToSimpleHeading() []*Heading {
	hdgs := NewHL()

	wg := sync.WaitGroup{}
	wg.Add(len(heading.Childs))
	for _, v := range heading.Childs {

		go func(v *HeadingAsDLList, wg *sync.WaitGroup) {
			hdgs.append(&Heading{
				ID:       v.ID,
				ParentID: heading.ID,
				Title:    v.Title,
				Level:    v.Level,
			})
			hdgs.append(v.ToSimpleHeading()...)
			wg.Done()
		}(v, &wg)
	}
	wg.Wait()
	return hdgs.slice
}

//Heading is struct as DB model
type Heading struct {
	ID       int64  `xml:"id" json:"id,omitempty" db:"id"`
	ParentID int64  `xml:"parent_id" json:"parent_id,omitempty" db:"parent_id"`
	Title    string `xml:"title" json:"title,omitempty" db:"title"`
	Level    int    `xml:"level" json:"level,omitempty" db:"level"`
}

type HeadingTable struct {
	DB interface {
		sqlx.Execer
		Get(interface{}, string, ...interface{}) error
		Select(interface{}, string, ...interface{}) error
	}
}

func (table *HeadingTable) FindByName(name string) (*Heading, error) {
	query := `select id, parent_id, title from heading where title = ?;`

	var heading Heading

	err := table.DB.Get(&heading, query, name)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &heading, nil

}

func (table *HeadingTable) FindAllChildByParentID(id int64) ([]Heading, error) {
	query := `select data.id, data.title 
				from heading as data
				join heading_tree as tree
				on data.id = tree.id_desc
				where tree.id_anc = ?;`

	var headings []Heading

	err := table.DB.Select(&headings, query, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return headings, nil
}
