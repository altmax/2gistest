package building

import (
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/altmax/2gistest/routes/response"

	"bitbucket.org/altmax/2gistest/models"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

const (
	groupPrefix = "/building"
)

var (
	availableRoutes = map[string]func(ctx echo.Context) error{
		"/all": GetAll,
		"/:id": GetByID,
	}
)

//SetGroupRoute creates sub_group and sets all available routes with GET methods
func SetGroupRoute(e *echo.Group) {
	g := e.Group(groupPrefix)
	for path, f := range availableRoutes {
		g.GET(path, f)
	}
}

//GetAll returns all buildings from DB
func GetAll(ctx echo.Context) error {
	buildings, err := (&models.BuildingTable{ctx.Get("DB").(*sqlx.DB)}).FindAll()
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	return ctx.JSON(http.StatusOK, response.NewResponse(buildings))
}

//GetByID returns building with defineds id from DB
func GetByID(ctx echo.Context) error {
	strID := ctx.Param("id")
	fmt.Println(strID)
	id, err := strconv.ParseInt(strID, 10, 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}

	bldg, err := (&models.BuildingTable{ctx.Get("DB").(*sqlx.DB)}).FindByID(id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if bldg == nil {
		return ctx.NoContent(http.StatusNotFound)
	}

	return ctx.JSON(http.StatusOK, response.NewResponse(bldg))
}
