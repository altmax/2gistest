package firm

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/altmax/2gistest/models"
	"bitbucket.org/altmax/2gistest/routes/response"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

const (
	groupPrefix = "/firm"
)

var (
	availableRoutes = map[string]func(ctx echo.Context) error{
		"/:id":                        GetByID,
		"/search/building/:id":        SearchByBlgID,
		"/search/center/:coord/:rad":  SearchAroundCenterByRad,
		"/search/center/:coord/:a/:b": SearchAroundCenterByRect,
		"/search/heading/:name":       SearchByHeadingName,
		"/search/heading/:name/deep":  DeepSearchByHeadingName,
	}
)

func SetGroupRoute(e *echo.Group) {
	g := e.Group(groupPrefix)
	for path, f := range availableRoutes {
		g.GET(path, f)
	}
}

func GetByID(ctx echo.Context) error {
	strID := ctx.Param("id")
	fmt.Println(strID)
	id, err := strconv.ParseInt(strID, 10, 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}

	firm, err := (&models.FirmTable{ctx.Get("DB").(*sqlx.DB)}).FindByID(id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if firm == nil {
		return ctx.NoContent(http.StatusNotFound)
	}

	return ctx.JSON(http.StatusOK, response.NewResponse(firm))
}

func SearchByBlgID(ctx echo.Context) error {
	strID := ctx.Param("id")

	id, err := strconv.ParseInt(strID, 10, 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}

	ctxDB := ctx.Get("DB").(*sqlx.DB)

	exist, err := (&models.BuildingTable{ctxDB}).Exist(id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if !exist {
		return ctx.NoContent(http.StatusNotFound)
	}

	firms, err := (&models.FirmTable{ctxDB}).FindByBuildingID(id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if len(firms) == 0 {
		return ctx.JSON(http.StatusOK, response.NewErrorResponse(errors.New("building is empty")))
	}

	return ctx.JSON(http.StatusOK, response.NewResponse(firms))
}

func SearchAroundCenterByRad(ctx echo.Context) error {
	center := ctx.Param("coord")
	radstr := ctx.Param("rad")

	rad, err := strconv.ParseInt(radstr, 10, 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}

	coords := strings.Split(center, ",")
	lat, err := strconv.ParseFloat(coords[0], 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}
	long, err := strconv.ParseFloat(strings.TrimSpace(coords[1]), 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}

	a, b := float64(rad), float64(rad)

	x1 := lat - (b/2)/(111.0*1000)
	x2 := lat + (b/2)/(111.0*1000)
	y1 := long - (a/2)/math.Abs(math.Cos(lat*math.Pi/180)*(111.0*1000))
	y2 := long + (a/2)/math.Abs(math.Cos(lat*math.Pi/180)*(111.0*1000))

	firms, err := (&models.FirmTable{ctx.Get("DB").(*sqlx.DB)}).FindByRad(x1, x2, y1, y2, lat, long, float64(rad))
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err)
	}
	if len(firms) == 0 {
		return ctx.JSON(http.StatusOK, response.NewErrorResponse(errors.New("round is empty")))
	}

	return ctx.JSON(http.StatusOK, response.NewResponse(firms))
}

func SearchAroundCenterByRect(ctx echo.Context) error {
	center := ctx.Param("coord")
	astr := ctx.Param("a")
	bstr := ctx.Param("b")
	a, err := strconv.ParseFloat(astr, 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}
	b, err := strconv.ParseFloat(bstr, 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}

	coords := strings.Split(center, ",")
	lat, err := strconv.ParseFloat(coords[0], 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}
	long, err := strconv.ParseFloat(strings.TrimSpace(coords[1]), 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, response.NewErrorResponse(err))
	}

	x1 := lat - (b/2)/(111.0*1000)
	x2 := lat + (b/2)/(111.0*1000)
	y1 := long - (a/2)/math.Abs(math.Cos(lat*math.Pi/180)*(111.0*1000))
	y2 := long + (a/2)/math.Abs(math.Cos(lat*math.Pi/180)*(111.0*1000))

	firms, err := (&models.FirmTable{ctx.Get("DB").(*sqlx.DB)}).FindByRect(x1, x2, y1, y2)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err)
	}
	if len(firms) == 0 {
		return ctx.JSON(http.StatusOK, response.NewErrorResponse(errors.New("heading is empty")))
	}

	return ctx.JSON(http.StatusOK, response.NewResponse(firms))
}

func SearchByHeadingName(ctx echo.Context) error {
	limitStr, offsetStr := ctx.FormValue("limit"), ctx.FormValue("offset")
	var limit, offset int64
	if limitStr != "" {
		limit, _ = strconv.ParseInt(limitStr, 10, 64)
	}
	if offsetStr != "" {
		offset, _ = strconv.ParseInt(offsetStr, 10, 64)
	}

	hdgName := ctx.Param("name")
	ctxDB := ctx.Get("DB").(*sqlx.DB)

	hTable := &models.HeadingTable{ctxDB}

	heading, err := hTable.FindByName(hdgName)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if heading == nil {
		return ctx.JSON(http.StatusNotFound, response.NewErrorResponse(errors.New("heading not found")))
	}

	firms, err := (&models.FirmTable{ctxDB}).FindByHeadingID(limit, offset, heading.ID)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if len(firms) == 0 {
		return ctx.JSON(http.StatusOK, response.NewErrorResponse(errors.New("heading is empty")))
	}

	return ctx.JSON(http.StatusOK, response.NewResponse(firms))
}

func DeepSearchByHeadingName(ctx echo.Context) error {
	limitStr, offsetStr := ctx.FormValue("limit"), ctx.FormValue("offset")
	var limit, offset int64
	if limitStr != "" {
		limit, _ = strconv.ParseInt(limitStr, 10, 64)
	}
	if offsetStr != "" {
		offset, _ = strconv.ParseInt(offsetStr, 10, 64)
	}

	hdgName := ctx.Param("name")
	ctxDB := ctx.Get("DB").(*sqlx.DB)

	hTable := &models.HeadingTable{ctxDB}

	rootHeading, err := hTable.FindByName(hdgName)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if rootHeading == nil {
		return ctx.JSON(http.StatusNotFound, response.NewErrorResponse(errors.New("heading not found")))
	}

	firms, err := (&models.FirmTable{ctxDB}).DeepFindByHeadingID(limit, offset, rootHeading.ID)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, response.NewErrorResponse(err))
	}
	if len(firms) == 0 {
		return ctx.JSON(http.StatusOK, response.NewErrorResponse(errors.New("heading is empty")))
	}

	return ctx.JSON(http.StatusOK, response.NewResponse(firms))
}
