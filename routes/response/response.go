package response

type Response struct {
	Response interface{} `xml:"response" json:"response,omitempty" db:"response"`
	Error    string      `xml:"error" json:"error,omitempty" db:"error"`
}

func NewResponse(vr interface{}) Response {
	return Response{Response: vr}
}

func NewErrorResponse(err error) Response {
	return Response{Error: err.Error()}
}
