package routes

import (
	"bitbucket.org/altmax/2gistest/routes/building"
	"bitbucket.org/altmax/2gistest/routes/firm"
	"github.com/labstack/echo"
)

var (
	availableRouteGroups = []func(*echo.Group){
		building.SetGroupRoute,
		firm.SetGroupRoute,
	}
)

type Response struct {
	Response interface{} `xml:"response" json:"response,omitempty" db:"response"`
	Error    error       `xml:"error" json:"error,omitempty" db:"error"`
}

func SetRoutes(e *echo.Group) {
	for _, g := range availableRouteGroups {
		g(e)
	}
}
