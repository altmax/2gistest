package server

import (
	"log"
	"net/http"
	"net/http/pprof"

	"bitbucket.org/altmax/2gistest/middlewares"
	"bitbucket.org/altmax/2gistest/routes"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
)

// New instantiates server
func New(config *viper.Viper) *echo.Echo {
	e := echo.New()

	if config.GetString("profile") == "http" {
		log.Println("Profiling enabled")
		e.GET("/debug/pprof/", echo.WrapHandler(http.HandlerFunc(pprof.Index)))
		e.GET("/debug/pprof/heap", echo.WrapHandler(pprof.Handler("heap")))
		e.GET("/debug/pprof/goroutine", echo.WrapHandler(pprof.Handler("goroutine")))
		e.GET("/debug/pprof/block", echo.WrapHandler(pprof.Handler("block")))
		e.GET("/debug/pprof/threadcreate", echo.WrapHandler(pprof.Handler("threadcreate")))
		e.GET("/debug/pprof/cmdline", echo.WrapHandler(http.HandlerFunc(pprof.Cmdline)))
		e.GET("/debug/pprof/profile", echo.WrapHandler(http.HandlerFunc(pprof.Profile)))
		e.GET("/debug/pprof/symbol", echo.WrapHandler(http.HandlerFunc(pprof.Symbol)))
	}

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	v1 := e.Group("/api/v1")
	v1.Use(middlewares.Mysql(config.GetString("mysql.master")))

	routes.SetRoutes(v1)
	return e
}
